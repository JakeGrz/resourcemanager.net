﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ResourceManager2._0.Reporting
{
    public class ResourceReport
    {
        private const string SELECT_ALL_RESOURCES = "SELECT LAST_NAME As 'Last Name', FIRST_NAME As 'First Name', SERIAL_NUMBER As 'Serial Number', EMAIL As 'Email', CURRENT_PROJECT As 'Current Project', PRIMARY_SKILL As 'Primary Skill', WORK_FROM_HOME As 'Work from Home', ACTIVE As 'Active' " +
                                                    "FROM RESOURCES " +
                                                    "ORDER BY LAST_NAME";
        
        public DataTable GetReportData(string connString)
        {
            SqlConnection conn = new SqlConnection(connString);
            SqlDataAdapter da = new SqlDataAdapter(SELECT_ALL_RESOURCES, conn); 
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds.Tables[0];    
        }

        public byte[] ExportToExcel(DataTable tbl)
        {
            byte[] export;
            string headerCells = "A3:H3";

            using (ExcelPackage xl = new ExcelPackage())         
            {
                ExcelWorksheet ws = xl.Workbook.Worksheets.Add("Resource List");
                //Create File Header
                ws.Cells["A1"].Value = "All Resources Report";
                ws.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                ws.Cells["A1"].Style.Font.Bold = true;
                ws.Cells["A1:H1"].Merge = true;
                ws.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //Place data in sheet
                ws.Cells["A3"].LoadFromDataTable(tbl, true);
                //Format sheet.
                ws.Cells.AutoFitColumns();
                ws.Cells[headerCells].AutoFilter = true;
                ws.Cells[headerCells].Style.Font.Bold = true;
                ws.Cells[headerCells].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[headerCells].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                var border = ws.Cells[headerCells].Style.Border;
                border.Bottom.Style = border.Top.Style = ExcelBorderStyle.Thick;
                ws.Cells["A3"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                ws.Cells["H3"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                //Convert sheet to byte array.
                export = xl.GetAsByteArray();
            }
            return export;            
        }
        
        
    }
}