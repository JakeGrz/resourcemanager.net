﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ResourceManager2._0.DataAccess;

namespace ResourceManager2._0
{
    public partial class MeetingAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ResourceManagerEntities context = new ResourceManagerEntities(MeetingDataSource.ConnectionString);
                //context.Connection.Open();
                DataAccess.MEETING m = new MEETING();

                m.MEETING_TITLE = textSubject.Text;
                m.MEETING_DATE = (System.DateTime) MtgDate.SelectedValue;
                m.MEETING_NOTES = txtNotes.Text;
                m.RESOURCE_ID = Convert.ToInt32(DropDownList1.SelectedValue);

                context.MEETINGS.AddObject(m);
                context.SaveChanges();
                Response.Redirect("~/Meetings/Meetings.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Meetings/Meetings.aspx");
        }

        
    }
}