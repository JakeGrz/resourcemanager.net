﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ResourceManager2._0.DataAccess;

namespace ResourceManager2._0
{
    public partial class MeetingDetail : System.Web.UI.Page
    {
        protected string meetingId;
        protected string resourceId;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            meetingId = Request.QueryString.Get(0);
            resourceId = Request.QueryString.Get(1);
        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ResourceManagerEntities context = new ResourceManagerEntities(CommentDataSource.ConnectionString);
                //context.Connection.Open();
                DataAccess.COMMENT c = new COMMENT();

                c.COMMENT_DATE = DateTime.Now;
                c.COMMENT_TEXT = txtComment.Text;
                c.MEETING_ID = Convert.ToInt32(meetingId);

                context.COMMENTS.AddObject(c);
                context.SaveChanges();
                Response.Redirect("~/Meetings/MeetingDetail.aspx?id=" + meetingId + "&rid=" + resourceId);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Meetings/MeetingAdd.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Meetings/Meetings.aspx");
        }
    }
}