﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MeetingDetail.aspx.cs" Inherits="ResourceManager2._0.MeetingDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 240px;
        }
    </style>
    <title>Resource Manager - Meeting Details</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style1">
        <tr>
            <td class="style2">
                <asp:EntityDataSource ID="ResourceDataSource" runat="server" ConnectionString="name=ResourceManagerEntities"
                    DefaultContainerName="ResourceManagerEntities" EnableFlattening="False" EntitySetName="RESOURCES"
                    EntityTypeFilter="RESOURCE" Select="it.[RESOURCE_ID], it.[FIRST_NAME], it.[LAST_NAME]"
                    AutoGenerateWhereClause="True" Where="">
                    <WhereParameters>
                        <asp:QueryStringParameter DefaultValue="1" Name="RESOURCE_ID" QueryStringField="rid"
                            Type="Int32" />
                    </WhereParameters>
                </asp:EntityDataSource>
                <asp:EntityDataSource ID="CommentDataSource" runat="server" ConnectionString="name=ResourceManagerEntities"
                    DefaultContainerName="ResourceManagerEntities" EnableFlattening="False" EnableInsert="True"
                    EntitySetName="COMMENTS" EntityTypeFilter="COMMENT" AutoGenerateWhereClause="True"
                    Select="" Where="">
                    <WhereParameters>
                        <asp:QueryStringParameter DefaultValue="1" Name="MEETING_ID" QueryStringField="id"
                            Type="Int32" />
                    </WhereParameters>
                </asp:EntityDataSource>
                <asp:EntityDataSource ID="MeetingDataSource" runat="server" ConnectionString="name=ResourceManagerEntities"
                    DefaultContainerName="ResourceManagerEntities" EnableFlattening="False" EntitySetName="MEETINGS"
                    AutoGenerateWhereClause="True" EntityTypeFilter="" Select="" Where="">
                    <WhereParameters>
                        <asp:QueryStringParameter DefaultValue="1" Name="MEETING_ID" QueryStringField="id"
                            Type="Int32" />
                    </WhereParameters>
                </asp:EntityDataSource>
                <h3>
                    Meeting Controls</h3>
                <p>
                    <asp:Button ID="btnNew" runat="server" Text="New Meeting" OnClick="btnNew_Click" /></p>
                <p>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" /></p>
            </td>
            <td>
                <h3>
                    Meeting Details</h3>
                <p>
                    Resource Information</p>
                <div class="detailsview">
                    <asp:DetailsView ID="dvResource" runat="server" AutoGenerateRows="False" DataSourceID="ResourceDataSource"
                        Height="50px" Width="250px">
                        <Fields>
                            <asp:BoundField DataField="FIRST_NAME" HeaderText="First Name:" ReadOnly="True" SortExpression="FIRST_NAME" />
                            <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name:" ReadOnly="True" SortExpression="LAST_NAME" />
                        </Fields>
                    </asp:DetailsView>
                </div>
                <p>
                    Meeting Information</p>
                <div class="detailsview">
                    <asp:DetailsView ID="dvMeeting" runat="server" Height="50px" Width="253px" AutoGenerateRows="False"
                        DataKeyNames="MEETING_ID" DataSourceID="MeetingDataSource">
                        <Fields>
                            <asp:BoundField DataField="MEETING_ID" HeaderText="Meeting ID:" ReadOnly="True" SortExpression="MEETING_ID" />
                            <asp:BoundField DataField="MEETING_TITLE" HeaderText="Meeting Subject:" SortExpression="MEETING_TITLE" />
                            <asp:BoundField DataField="MEETING_DATE" HeaderText="Meeting Date:" SortExpression="MEETING_DATE" />
                            <asp:BoundField DataField="MEETING_NOTES" HeaderText="Notes:" SortExpression="MEETING_NOTES" />
                            <asp:BoundField DataField="RESOURCE_ID" HeaderText="RESOURCE_ID" SortExpression="RESOURCE_ID"
                                Visible="False" />
                        </Fields>
                    </asp:DetailsView>
                </div>
                <p>
                    Comments</p>
                <asp:Repeater ID="rpComments" runat="server" DataSourceID="CommentDataSource">
                    <ItemTemplate>
                        <p>
                            Date:
                            <%#DataBinder.Eval(Container.DataItem, "COMMENT_DATE") %></p>
                        <p>
                            <%#DataBinder.Eval(Container.DataItem, "COMMENT_TEXT") %></p>
                    </ItemTemplate>
                </asp:Repeater>
                <p>
                    Add New Comment</p>
                <br />
                <table class="style1">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="New Comment:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtComment" runat="server" Height="50px" TextMode="MultiLine" Width="267px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnComment" runat="server" OnClick="btnComment_Click" Text="Add Comment" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
