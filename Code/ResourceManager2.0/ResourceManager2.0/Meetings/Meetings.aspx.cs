﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ResourceManager2._0
{
    public partial class Meetings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string id = GridView1.Rows[e.NewSelectedIndex].Cells[0].Text;
            string rid = DropDownList1.SelectedItem.Value.ToString();
            Response.Redirect("~/Meetings/MeetingDetail.aspx?id=" + id + "&rid=" + rid);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Meetings/MeetingAdd.aspx");
        }
    }
}