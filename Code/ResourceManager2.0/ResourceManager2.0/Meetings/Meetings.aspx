﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Meetings.aspx.cs" Inherits="ResourceManager2._0.Meetings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Resource Manager - All Meetings</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 238px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style1">
        <tr>
            <td class="style2">
                <asp:EntityDataSource ID="ResourceDataSource" runat="server" ContextTypeName="ResourceManager2._0.DataAccess.ResourceManagerEntities"
                    EnableFlattening="False" EntitySetName="RESOURCES" EntityTypeFilter="RESOURCE"
                    Select="it.[RESOURCE_ID], it.[FULL_NAME]" ConnectionString="" DefaultContainerName=""
                    OrderBy="it.[FULL_NAME]">
                </asp:EntityDataSource>
                <asp:EntityDataSource ID="MeetingDataSource" runat="server" ContextTypeName="ResourceManager2._0.DataAccess.ResourceManagerEntities"
                    EnableFlattening="False" EntitySetName="MEETINGS" AutoGenerateWhereClause="True"
                    ConnectionString="" DefaultContainerName="" EntityTypeFilter="" Select="" Where="">
                    <WhereParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="RESOURCE_ID" PropertyName="SelectedValue"
                            Type="Int32" />
                    </WhereParameters>
                </asp:EntityDataSource>
                <h3>
                    Meeting Controls</h3>
                <p>
                    Select Resource</p>
                <p>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ResourceDataSource"
                        DataTextField="FULL_NAME" DataValueField="RESOURCE_ID" AutoPostBack="True">
                    </asp:DropDownList>
                </p>
                <p>
                    <asp:Button ID="btnNew" runat="server" Text="New Meeting" OnClick="btnNew_Click" /></p>
            </td>
            <td>
                <h3>
                    Meetings</h3>
                <div class="datagrid">
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyNames="MEETING_ID" DataSourceID="MeetingDataSource"
                        OnSelectedIndexChanging="GridView1_SelectedIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="MEETING_ID" HeaderText="Meeting ID" ReadOnly="True" SortExpression="MEETING_ID" />
                            <asp:BoundField DataField="MEETING_TITLE" HeaderText="Subject" SortExpression="MEETING_TITLE" />
                            <asp:BoundField DataField="MEETING_DATE" HeaderText="Date" SortExpression="MEETING_DATE" />
                            <asp:BoundField DataField="RESOURCE_ID" HeaderText="RESOURCE_ID" SortExpression="RESOURCE_ID"
                                Visible="False" />
                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
