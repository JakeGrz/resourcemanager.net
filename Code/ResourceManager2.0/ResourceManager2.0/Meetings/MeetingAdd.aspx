﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MeetingAdd.aspx.cs" Inherits="ResourceManager2._0.MeetingAdd" %>

<%@ Register TagPrefix="bdp" Namespace="BasicFrame.WebControls" Assembly="BasicFrame.WebControls.BasicDatePicker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Resource Manager - New Meeting</title>
    <style type="text/css">
        .style1
        {
            width: 98%;
        }
        .style2
        {
            width: 238px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style1">
        <tr>
            <td class="style2">
                <asp:EntityDataSource ID="ResourceDataSource" runat="server" ContextTypeName="ResourceManager2._0.DataAccess.ResourceManagerEntities"
                    EnableFlattening="False" EntitySetName="RESOURCES" EntityTypeFilter="RESOURCE"
                    Select="it.[RESOURCE_ID], it.[FULL_NAME]">
                </asp:EntityDataSource>
                <asp:EntityDataSource ID="MeetingDataSource" runat="server" ContextTypeName="ResourceManager2._0.DataAccess.ResourceManagerEntities"
                    EnableFlattening="False" EntitySetName="MEETINGS" EnableInsert="True" EntityTypeFilter="MEETING"
                    ConnectionString="name=ResourceManagerEntities" DefaultContainerName="ResourceManagerEntities">
                </asp:EntityDataSource>
                <h3>
                    Meeting Controls</h3>
                <p>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                        OnClick="btnCancel_Click" CausesValidation="False" /></p>
            </td>
            <td>
                <h3>
                    New Meeting</h3>
                <table class="style1">
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label1" runat="server" Text="Resource:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ResourceDataSource"
                                DataTextField="FULL_NAME" DataValueField="RESOURCE_ID" Height="16px" Width="232px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownList1"
                                ErrorMessage="Resource is required." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label3" runat="server" Text="Meeting Subject:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="textSubject" runat="server" Width="221px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="textSubject"
                                ErrorMessage="Meeting Subject is required." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label4" runat="server" Text="Meeting Date:"></asp:Label>
                        </td>
                        <td class="style4">
                            <bdp:BDPLite ID="MtgDate" runat="server">
                            </bdp:BDPLite>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="MtgDate"
                                ErrorMessage="Meeting Date is required." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label5" runat="server" Text="Meeting Notes:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtNotes" runat="server" Width="228px" Height="249px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNotes"
                                ErrorMessage="Meeting Notes are required." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;
                        </td>
                        <td class="style4">
                            <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Save Meeting" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
