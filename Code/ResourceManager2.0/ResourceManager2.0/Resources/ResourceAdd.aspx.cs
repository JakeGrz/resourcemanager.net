﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ResourceManager2._0.DataAccess;

namespace ResourceManager2._0
{
    public partial class ResourceAdd1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Check validation
            if (Page.IsValid)
            {
                //Get connection to entity data source.
                ResourceManagerEntities context = new ResourceManagerEntities(ResourceDataSource.ConnectionString);
                //Setup RESOURCE to update. 
                DataAccess.RESOURCE r = new RESOURCE();
                r.FIRST_NAME = txtFirstName.Text;
                r.LAST_NAME = txtLastName.Text;
                r.SERIAL_NUMBER = txtSerial.Text;
                r.EMAIL = txtEmail.Text;
                if (txtPrimarySkill.Text == "")
                    r.PRIMARY_SKILL = "N/A";
                else
                    r.PRIMARY_SKILL = txtPrimarySkill.Text;
                if (txtCurrProject.Text == "")
                    r.CURRENT_PROJECT = "Bench";
                else
                    r.CURRENT_PROJECT = txtCurrProject.Text;
                r.WORK_FROM_HOME = chkWorkFromHome.Checked;
                r.ACTIVE = true;
                r.FULL_NAME = txtLastName.Text + ", " + txtFirstName.Text;
                //Perform table update.
                context.RESOURCES.AddObject(r);
                context.SaveChanges();
                //Return to All Resources screen.
                Response.Redirect("~/Resources/Resources.aspx");
            }           
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Resources/Resources.aspx");
        }

        protected void validateSerialLength_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidateSerialLength(txtSerial.Text);
        }

        private bool ValidateSerialLength(string serial)
        {
            if (serial.Length > 6)
                return false;
            else
                return true;
        }
    }
}