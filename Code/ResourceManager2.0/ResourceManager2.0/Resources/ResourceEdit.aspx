﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ResourceEdit.aspx.cs" Inherits="ResourceManager2._0.ResourceAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 201px;
        }
    </style>
    <title>Resource Manager - Resource Details</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:EntityDataSource ID="ResourceDataSource" runat="server" ContextTypeName="ResourceManager2._0.DataAccess.ResourceManagerEntities"
        EnableFlattening="False" EnableUpdate="True" EntitySetName="RESOURCES" EntityTypeFilter="RESOURCE"
        Where="" AutoGenerateWhereClause="True" ConnectionString="" DefaultContainerName=""
        Select="">
        <WhereParameters>
            <asp:QueryStringParameter DefaultValue="1" Name="RESOURCE_ID" QueryStringField="id"
                Type="Int32" />
        </WhereParameters>
    </asp:EntityDataSource>
    <table class="style1">
        <tr>
            <td class="style2">
                <h3>
                    Resource Controls</h3>
                <p>
                    <asp:Button ID="btnNew" runat="server" Text="New Resource" OnClick="btnNew_Click" /></p>
                <p>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" /></p>
            </td>
            <td>
                <h2>
                    Resource Details</h2>
                <div class="detailsview">
                    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="RESOURCE_ID"
                        DataSourceID="ResourceDataSource" Height="50px" Width="439px">
                        <Fields>
                            <asp:BoundField DataField="RESOURCE_ID" HeaderText="ID:" ReadOnly="True" SortExpression="RESOURCE_ID" />
                            <asp:BoundField DataField="FIRST_NAME" HeaderText="First Name:" SortExpression="FIRST_NAME" />
                            <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name:" SortExpression="LAST_NAME" />
                            <asp:BoundField DataField="SERIAL_NUMBER" HeaderText="Serial #:" SortExpression="SERIAL_NUMBER" />
                            <asp:BoundField DataField="EMAIL" HeaderText="Email:" SortExpression="EMAIL" />
                            <asp:BoundField DataField="CURRENT_PROJECT" HeaderText="Current Project:" SortExpression="CURRENT_PROJECT" />
                            <asp:BoundField DataField="PRIMARY_SKILL" HeaderText="Primary Skill:" SortExpression="PRIMARY_SKILL" />
                            <asp:CheckBoxField DataField="WORK_FROM_HOME" HeaderText="Work from Home:" SortExpression="WORK_FROM_HOME" />
                            <asp:CheckBoxField DataField="ACTIVE" HeaderText="Active:" SortExpression="ACTIVE" />
                            <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                        </Fields>
                    </asp:DetailsView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
