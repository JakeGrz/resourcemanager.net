﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Resources.aspx.cs" Inherits="ResourceManager2._0.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 201px;
        }
    </style>
    <title>Resource Manager - All Resources</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:EntityDataSource ID="EntityDataSource1" runat="server" ContextTypeName="ResourceManager2._0.DataAccess.ResourceManagerEntities"
        EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="RESOURCES"
        EntityTypeFilter="RESOURCE" ConnectionString="name=ResourceManagerEntities" DefaultContainerName="ResourceManagerEntities">
    </asp:EntityDataSource>
    <table class="style1">
        <tr>
            <td class="style2">
                <h3>
                    Resource Controls</h3>
                <p>
                    <asp:Button ID="btnNew" runat="server" Text="New Resource" 
                        onclick="btnNew_Click" /></p>
                        <p><asp:Button ID="btnExport" runat="server" Text="Export to Excel File" 
                                onclick="btnExport_Click" /></p>
                
            </td>
            <td>
                <h3>
                    All Resources</h3>
                <div class="datagrid">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyNames="RESOURCE_ID" DataSourceID="EntityDataSource1"
                    OnSelectedIndexChanging="GridView1_SelectedIndexChanging" Width="696px">
                    <Columns>
                        <asp:BoundField DataField="RESOURCE_ID" HeaderText="ID" ReadOnly="True" SortExpression="RESOURCE_ID" />
                        <asp:BoundField DataField="FIRST_NAME" HeaderText="Last Name" SortExpression="FIRST_NAME" />
                        <asp:BoundField DataField="LAST_NAME" HeaderText="First Name" SortExpression="LAST_NAME" />
                        <asp:BoundField DataField="SERIAL_NUMBER" HeaderText="Serial #" SortExpression="SERIAL_NUMBER" />
                        <asp:BoundField DataField="EMAIL" HeaderText="Email" SortExpression="EMAIL" Visible="False" />
                        <asp:BoundField DataField="CURRENT_PROJECT" HeaderText="Current Project" SortExpression="CURRENT_PROJECT" />
                        <asp:BoundField DataField="PRIMARY_SKILL" HeaderText="Primary Skill" SortExpression="PRIMARY_SKILL" />
                        <asp:CheckBoxField DataField="WORK_FROM_HOME" HeaderText="Work from Home" SortExpression="WORK_FROM_HOME"
                            Visible="False" />
                        <asp:CheckBoxField DataField="ACTIVE" HeaderText="ACTIVE" SortExpression="ACTIVE"
                            Visible="False" />
                        <asp:CommandField ShowSelectButton="True" ButtonType="Button" />
                    </Columns>
                    <FooterStyle BorderStyle="None" />
                    <HeaderStyle BorderStyle="None" />
                    <PagerStyle BorderStyle="None" />
                    <RowStyle BorderStyle="None" />
                    <SelectedRowStyle BorderStyle="None" />
                </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
