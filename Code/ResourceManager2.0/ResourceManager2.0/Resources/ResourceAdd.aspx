﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResourceAdd.aspx.cs" Inherits="ResourceManager2._0.ResourceAdd1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Resource Manager - Add New Resource</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 240px;
        }
        .style3
        {
            width: 115px;
        }
        .style4
        {
            width: 241px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
    <table class="style1">
        <tr>
            <td class="style2">
                <asp:EntityDataSource ID="ResourceDataSource" runat="server" 
                    ConnectionString="name=ResourceManagerEntities" 
                    DefaultContainerName="ResourceManagerEntities" EnableFlattening="False" 
                    EnableInsert="True" EntitySetName="RESOURCES" EntityTypeFilter="RESOURCE">
                </asp:EntityDataSource>
                <h3>Resource Controls</h3>
                <p><asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" CausesValidation="False" /></p>
                
            </td>
            <td>
                <h3>Add New Resource</h3>
                <table class="style1">
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label1" runat="server" Text="First Name:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtFirstName" runat="server" Width="227px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="validateFirstName" runat="server" 
                                ControlToValidate="txtFirstName" ErrorMessage="First name is required." 
                                ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label2" runat="server" Text="Last Name:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtLastName" runat="server" Width="227px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="validateLastName" runat="server" 
                                ControlToValidate="txtLastName" ErrorMessage="Last name is required." 
                                ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label3" runat="server" Text="Serial #:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtSerial" runat="server" Width="229px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="validateSerialNumber" runat="server" 
                                ControlToValidate="txtSerial" ErrorMessage="Serial Number is required." 
                                ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                <br />
                            <asp:CustomValidator ID="validateSerialLength" runat="server" 
                                ControlToValidate="txtSerial" 
                                ErrorMessage="Serial number must be 6 characters." ForeColor="#CC0000" 
                                onservervalidate="validateSerialLength_ServerValidate"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label4" runat="server" Text="Email:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtEmail" runat="server" Width="228px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="validateEmail" runat="server" 
                                ControlToValidate="txtEmail" Display="Dynamic" 
                                ErrorMessage="Email is required." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="validateEmailFormat" runat="server" 
                                ControlToValidate="txtEmail" Display="Dynamic" 
                                ErrorMessage="Enter valid email address." ForeColor="#CC0000" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label5" runat="server" Text="Current Project:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtCurrProject" runat="server" Width="228px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label6" runat="server" Text="Primary Skill:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="txtPrimarySkill" runat="server" Width="230px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="Label7" runat="server" Text="Work from Home:"></asp:Label>
                        </td>
                        <td class="style4">
                            <asp:CheckBox ID="chkWorkFromHome" runat="server" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;</td>
                        <td class="style4">
                            <asp:Button ID="btnAdd" runat="server" onclick="btnAdd_Click" 
                                Text="Add Resource" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
 
</asp:Content>
