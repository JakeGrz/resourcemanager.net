﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ResourceManager2._0.Reporting;
using ResourceManager2._0.DataAccess;
using OfficeOpenXml;
using System.Data;

namespace ResourceManager2._0
{
    public partial class Resources : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string id = GridView1.Rows[e.NewSelectedIndex].Cells[0].Text;
            Response.Redirect("~/Resources/ResourceEdit.aspx?id=" + id);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Resources/ResourceAdd.aspx");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            ResourceReport rpt = new ResourceReport();
            DataTable tbl = rpt.GetReportData(GetConnString());
            
            //Write it back to the client
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;  filename=AllResources_" + DateTime.Now.ToString("mmddyyyy") + ".xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(rpt.ExportToExcel(tbl));
            Response.End(); 
        }

        private string GetConnString()
        {
            System.Configuration.Configuration rootWebConfig =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("\\Web.config");
            if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
                return rootWebConfig.ConnectionStrings.ConnectionStrings["ResourceManager"].ConnectionString;
            else
                return "";
        }
        
    }
}