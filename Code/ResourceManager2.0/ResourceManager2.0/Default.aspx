﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ResourceManager2._0._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <title>Resource Manager - Home</title>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to Resource Manager!
    </h2>
    <p>
        Resource Manager is a tool to track meetings with employees.
    </p>
    <p>
        Click Login to log into the application or to register your user account.
    </p>
    <h3>
        Resources</h3>
    <p>
        The Resources item on the menu will allow you to add new resources, view all resources,
        and view and edit resource details.
    </p>
    <h3>
        Meetings</h3>
    <p>
        The Meeting item on the menu will allow you to create a new meeting with a resource,
        view all meetings with a resource, and view meeting details. Comments can allow
        be added to meetings.</p>
</asp:Content>
