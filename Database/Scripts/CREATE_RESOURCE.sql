-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jake Grzegorski
-- Create date: 2/18/2013
-- Description:	Creates a new rsource record.
-- =============================================
CREATE PROCEDURE [dbo].[CREATE_RESOURCE] 
	@FirstName						varchar(15),
	@LastName						varchar(30),
	@SerialNumber					varchar(6),
	@Email							varchar(50),
	@CurrentProject					varchar(50),
	@PrimarySkill					varchar(20),
	@WorkFromHome					bit,
	@Active							bit,
	@FullName						varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.RESOURCES
	(	FIRST_NAME,
		LAST_NAME,
		SERIAL_NUMBER,
		EMAIL,
		CURRENT_PROJECT,
		PRIMARY_SKILL,
		WORK_FROM_HOME,
		ACTIVE,
		FULL_NAME )
	VALUES
	(	@FirstName,
		@LastName,
		@SerialNumber,
		@Email,
		@CurrentProject,
		@PrimarySkill,
		@WorkFromHome,
		@Active,
		@FullName )
	
	
END
GO
