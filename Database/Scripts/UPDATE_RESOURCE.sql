-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jake Grzegorski
-- Create date: 3/5/2013
-- Description:	Handles updates to the RESOURCES 
--				table.
-- =============================================
CREATE PROCEDURE [dbo].[UPDATE_RESOURCE] 
	@Id								int,
	@FirstName						varchar(15),
	@LastName						varchar(30),
	@SerialNumber					varchar(6),
	@Email							varchar(50),
	@CurrentProject					varchar(50),
	@PrimarySkill					varchar(20),
	@WorkFromHome					bit,
	@Active							bit,
	@FullName						varchar(50)
AS
BEGIN
	UPDATE dbo.RESOURCES
	SET
		FIRST_NAME = @FirstName,
		LAST_NAME = @LastName,
		SERIAL_NUMBER = @SerialNumber,
		EMAIL = @Email,
		CURRENT_PROJECT = @CurrentProject,
		PRIMARY_SKILL = @PrimarySkill,
		WORK_FROM_HOME = @WorkFromHome,
		ACTIVE = @Active,
		FULL_NAME = @FullName
	WHERE
		RESOURCE_ID = @Id;
END
GO
